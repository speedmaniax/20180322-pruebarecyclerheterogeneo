package com.example.juanp.a20180322_pruebarecyclerviewheterogeneo;

import android.content.Context;
import android.provider.Telephony;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by juanp on 22/03/2018.
 */

public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_CALL = 0, TYPE_SMS = 1;
    List<Object> SMSLlamadas = new ArrayList<>();
    private Context context;

    public Adapter(Context context) {
        this.context = context;
    }

    public void alimentador(List<Object> SMSLlamadas) {
        this.SMSLlamadas = SMSLlamadas;

    }


    public int getItemViewType(int position) {
        if (SMSLlamadas.get(position) instanceof Llamada) {
            return TYPE_CALL;
        } else {
            return TYPE_SMS;
        }
    }


    //se llama al crear vistas para inflarlas
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout;
        RecyclerView.ViewHolder viewHolder;

        switch (viewType) {
            case TYPE_CALL:
                layout = R.layout.item_llamada;
                View vistaLlamadas = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
                viewHolder = new ViewHolderLlamada(vistaLlamadas);


                break;

            case TYPE_SMS:
                layout = R.layout.item_sms;
                View vistaSMS = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
                viewHolder = new ViewHolderSMS(vistaSMS);


                break;

            default:
                viewHolder = null;
        }


        return viewHolder;
    }

    //se llama a este metodo cuando se necesite cargar los detalles de las vistas una vez infladas
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();


        switch (viewType) {
            case TYPE_CALL:
                Llamada llamada = (Llamada) SMSLlamadas.get(position);
                ((ViewHolderLlamada) holder).detallesLlamada(llamada);
                break;

            case TYPE_SMS:
                SMS sms = (SMS) SMSLlamadas.get(position);
                ((ViewHolderSMS) holder).detallesSMS(sms);
                break;
        }


    }


    //devolvemos el tamaño de la lista de objetos
    @Override
    public int getItemCount() {
        return SMSLlamadas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);


        }
    }

    public class ViewHolderLlamada extends RecyclerView.ViewHolder {
        TextView tv_nombre;
        TextView tv_hora;
        ConstraintLayout parentLlamada;
        ImageButton ibtn_borrar;
        ImageButton ibtn_subir;
        ImageButton ibtn_bajar;


        public ViewHolderLlamada(View itemView) {
            super(itemView);
            tv_nombre = itemView.findViewById(R.id.tv_nombre_llamada);
            tv_hora = itemView.findViewById(R.id.tv_hora_llamada);
            parentLlamada = itemView.findViewById(R.id.parent_llamadada);
            ibtn_bajar = itemView.findViewById(R.id.ibtn_bajar_llamada);
            ibtn_subir = itemView.findViewById(R.id.ibtn_subir_llamada);
            ibtn_borrar = itemView.findViewById(R.id.ibtn_borrar_llamada);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();
                    Toast.makeText(context, "hola " + position, Toast.LENGTH_SHORT).show();
                }
            });

//            itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View view) {
//
//                    int position = getLayoutPosition();
//                    Toast.makeText(context, "hola " + position, Toast.LENGTH_SHORT).show();
//                    SMSLlamadas.remove(position);
//                    notifyDataSetChanged();
//
//                    return false;
//                }
//            });


            ibtn_borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();

                    SMSLlamadas.remove(position);
                    notifyDataSetChanged();
                }
            });

            ibtn_subir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();

                    if(position>0){
                        Collections.swap(SMSLlamadas, position, position-1);

                    }

                    notifyDataSetChanged();
                }
            });

            ibtn_bajar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();
                    if(position<SMSLlamadas.size()-1){
                        Collections.swap(SMSLlamadas, position, position+1);

                    }
                    notifyDataSetChanged();
                }
            });


        }

        public void detallesLlamada(Llamada llamada) {
            tv_nombre.setText(llamada.getNombre());
            tv_hora.setText(llamada.getHora());
        }
    }

    public class ViewHolderSMS extends RecyclerView.ViewHolder {
        TextView tv_nombre;
        TextView tv_hora;
        TextView tv_mensaje;
        ImageButton ibtn_borrar;
        ImageButton ibtn_subir;
        ImageButton ibtn_bajar;
        ConstraintLayout parentSMS;

        public ViewHolderSMS(View itemView) {
            super(itemView);
            tv_nombre = itemView.findViewById(R.id.tv_nombre_sms);
            tv_hora = itemView.findViewById(R.id.tv_hora_sms);
            tv_mensaje = itemView.findViewById(R.id.tv_mensaje);
            parentSMS = itemView.findViewById(R.id.parent_sms);
            ibtn_bajar = itemView.findViewById(R.id.ibtn_bajar_sms);
            ibtn_subir = itemView.findViewById(R.id.ibtn_subir_sms);
            ibtn_borrar = itemView.findViewById(R.id.ibtn_borrar_sms);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();
                    Toast.makeText(context, "hola " + position, Toast.LENGTH_SHORT).show();
                }
            });


//            itemView.setOnLongClickListener(new View.OnLongClickListener() {
//                @Override
//                public boolean onLongClick(View view) {
//                    int position = getLayoutPosition();
//                    Toast.makeText(context, "hola " + position, Toast.LENGTH_SHORT).show();
//
//                    SMSLlamadas.remove(position);
//                    notifyDataSetChanged();
//                    return false;
//                }
//            });

            ibtn_subir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();
                    if (position > 0) {
                        Collections.swap(SMSLlamadas, position, position - 1);
                    }

                    notifyDataSetChanged();
                }
            });

            ibtn_bajar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();
                    if (position < SMSLlamadas.size() - 1) {
                        Collections.swap(SMSLlamadas, position, position + 1);
                    }

                    notifyDataSetChanged();
                }
            });

            ibtn_borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getLayoutPosition();
                    SMSLlamadas.remove(position);
                    notifyDataSetChanged();
                }
            });
        }

        public void detallesSMS(SMS sms) {
            tv_nombre.setText(sms.getNombre());
            tv_hora.setText(sms.getHora());
            tv_mensaje.setText(sms.getMensaje());
        }
    }


}
