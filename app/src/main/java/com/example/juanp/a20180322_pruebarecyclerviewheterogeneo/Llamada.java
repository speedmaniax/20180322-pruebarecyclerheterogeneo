package com.example.juanp.a20180322_pruebarecyclerviewheterogeneo;

/**
 * Created by juanp on 22/03/2018.
 */

public  class Llamada {
    private String nombre;
    private String hora;

    public Llamada(String nombre, String hora) {
        this.nombre = nombre;
        this.hora = hora;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }


}