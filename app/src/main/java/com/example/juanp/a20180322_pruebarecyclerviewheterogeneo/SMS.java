package com.example.juanp.a20180322_pruebarecyclerviewheterogeneo;

/**
 * Created by juanp on 22/03/2018.
 */

public class SMS{
    private String nombre;
    private String hora;
    private String mensaje;

    public SMS(String nombre, String hora, String mensaje) {
        this.nombre = nombre;
        this.hora = hora;
        this.mensaje = mensaje;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }


}