package com.example.juanp.a20180322_pruebarecyclerviewheterogeneo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {


    List<Object> SMSLlamadas;
    RecyclerView recyclerView;
    Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerview);

        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));
        init();
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                getResources().getConfiguration().orientation);
        recyclerView.addItemDecoration(dividerItemDecoration);




    }

    void init() {

        SMSLlamadas = new ArrayList<>();
        adapter = new Adapter(getApplicationContext());
        recyclerView.setAdapter(adapter);

        SMSLlamadas.add(new Llamada("Pepe", "10:02 AM"));
        SMSLlamadas.add(new Llamada("Juan", "10:03 AM"));
        SMSLlamadas.add(new Llamada("Pedro", "10:05 AM"));
        SMSLlamadas.add(new SMS("Juan", "10:05 AM", "eii ola ke tal"));
        SMSLlamadas.add(new Llamada("Jesus", "10:30 AM"));
        SMSLlamadas.add(new SMS("Jesus", "10:30 AM", "locooooooooo"));
        SMSLlamadas.add(new Llamada("Santi", "10:30 AM"));
        SMSLlamadas.add(new Llamada("Pedro", "11:20 AM"));
        SMSLlamadas.add(new SMS("Pedro", "10:30 AM", "yeeeeee ke dise"));
        SMSLlamadas.add(new Llamada("Juan", "11:43 AM"));
        SMSLlamadas.add(new Llamada("Pepe", "01:23 PM"));
        SMSLlamadas.add(new Llamada("Pedro", "02:40 PM"));
        SMSLlamadas.add(new SMS("Pepe", "03:23 PM", "eeee que ise"));
        SMSLlamadas.add(new SMS("Santi", "04:24 PM", "no se tio toi rayao"));
        SMSLlamadas.add(new Llamada("Jesus", "05:03 PM"));
        SMSLlamadas.add(new SMS("Maria", "06:05 PM", "eii ke tal"));
        SMSLlamadas.add(new SMS("Santi", "07:20 PM", "ola wenas no estoi mu puesto en sto d los fors"));
        SMSLlamadas.add(new Llamada("Pedro", "08:00 PM"));
        SMSLlamadas.add(new SMS("Maria", "08:02 PM", "wenas ke ases"));

        adapter.alimentador(SMSLlamadas);
        adapter.notifyDataSetChanged();


    }
}
